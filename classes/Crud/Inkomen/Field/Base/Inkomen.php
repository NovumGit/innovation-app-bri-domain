<?php
namespace Crud\Custom\NovumBri\Inkomen\Field\Base;

use Crud\Generic\Field\GenericMoney;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'inkomen' crud field from the 'inkomen' table.
 * This class is auto generated and should not be modified.
 */
abstract class Inkomen extends GenericMoney implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'inkomen';

	protected $sFieldLabel = 'Inkomen';

	protected $sIcon = 'money';

	protected $sPlaceHolder = 'Belastbaar inkomen';

	protected $sGetter = 'getInkomen';

	protected $sFqModelClassname = '\Model\Custom\NovumBri\Inkomen';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['inkomen']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Inkomen" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
