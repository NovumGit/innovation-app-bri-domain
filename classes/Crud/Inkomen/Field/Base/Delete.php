<?php 
namespace Crud\Custom\NovumBri\Inkomen\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumBri\Inkomen;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Inkomen)
		{
		     return "/custom/novumbri/belastingen/inkomen/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Inkomen)
		{
		     return "/custom/novumbri/inkomen?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
