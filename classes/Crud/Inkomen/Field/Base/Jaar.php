<?php
namespace Crud\Custom\NovumBri\Inkomen\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'jaar' crud field from the 'inkomen' table.
 * This class is auto generated and should not be modified.
 */
abstract class Jaar extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'jaar';

	protected $sFieldLabel = 'Start datum';

	protected $sIcon = 'calendar';

	protected $sPlaceHolder = 'Jaar waarover aangifte is gedaan';

	protected $sGetter = 'getJaar';

	protected $sFqModelClassname = '\Model\Custom\NovumBri\Inkomen';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['jaar']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Start datum" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
