<?php
namespace Crud\Custom\NovumBri\Inkomen\Field;

use Crud\Custom\NovumBri\Inkomen\Field\Base\Jaar as BaseJaar;

/**
 * Skeleton subclass for representing jaar field from the inkomen table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Jaar extends BaseJaar
{
}
