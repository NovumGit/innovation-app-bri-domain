<?php
namespace Crud\Custom\NovumBri\Inkomen\Base;

use Crud\Custom\NovumBri;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumBri\Inkomen;
use Model\Custom\NovumBri\InkomenQuery;
use Model\Custom\NovumBri\Map\InkomenTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Inkomen instead if you need to override or add functionality.
 */
abstract class CrudInkomenManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumBri\CrudTrait;
	use NovumBri\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return InkomenQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumBri\Map\InkomenTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint bevat fake inkomsten gegevens.";
	}


	public function getEntityTitle(): string
	{
		return "Inkomen";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumbri/belastingen/inkomen/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumbri/belastingen/inkomen/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Inkomstenbelasting toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Inkomstenbelasting aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Inkomen', 'Jaar', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Inkomen', 'Jaar'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Inkomen
	 */
	public function getModel(array $aData = null): Inkomen
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oInkomenQuery = InkomenQuery::create();
		     $oInkomen = $oInkomenQuery->findOneById($aData['id']);
		     if (!$oInkomen instanceof Inkomen) {
		         throw new LogicException("Inkomen should be an instance of Inkomen but got something else." . __METHOD__);
		     }
		     $oInkomen = $this->fillVo($aData, $oInkomen);
		} else {
		     $oInkomen = new Inkomen();
		     if (!empty($aData)) {
		         $oInkomen = $this->fillVo($aData, $oInkomen);
		     }
		}
		return $oInkomen;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Inkomen
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Inkomen
	{
		$oInkomen = $this->getModel($aData);


		 if(!empty($oInkomen))
		 {
		     $oInkomen = $this->fillVo($aData, $oInkomen);
		     $oInkomen->save();
		 }
		return $oInkomen;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Inkomen $oModel
	 * @return Inkomen
	 */
	protected function fillVo(array $aData, Inkomen $oModel): Inkomen
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['inkomen']) ? $oModel->setInkomen($aData['inkomen']) : null;
		isset($aData['jaar']) ? $oModel->setJaar($aData['jaar']) : null;
		return $oModel;
	}
}
