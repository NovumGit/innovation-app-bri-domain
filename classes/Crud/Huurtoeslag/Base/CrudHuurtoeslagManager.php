<?php
namespace Crud\Custom\NovumBri\Huurtoeslag\Base;

use Crud\Custom\NovumBri;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumBri\Huurtoeslag;
use Model\Custom\NovumBri\HuurtoeslagQuery;
use Model\Custom\NovumBri\Map\HuurtoeslagTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Huurtoeslag instead if you need to override or add functionality.
 */
abstract class CrudHuurtoeslagManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumBri\CrudTrait;
	use NovumBri\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return HuurtoeslagQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumBri\Map\HuurtoeslagTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint bevat fake huurtoeslag gegevens.";
	}


	public function getEntityTitle(): string
	{
		return "Huurtoeslag";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumbri/toeslagen/huurtoeslag/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumbri/toeslagen/huurtoeslag/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Huurtoeslag toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Huurtoeslag aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Huurtoeslag', 'StartDatum', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Huurtoeslag', 'StartDatum'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Huurtoeslag
	 */
	public function getModel(array $aData = null): Huurtoeslag
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oHuurtoeslagQuery = HuurtoeslagQuery::create();
		     $oHuurtoeslag = $oHuurtoeslagQuery->findOneById($aData['id']);
		     if (!$oHuurtoeslag instanceof Huurtoeslag) {
		         throw new LogicException("Huurtoeslag should be an instance of Huurtoeslag but got something else." . __METHOD__);
		     }
		     $oHuurtoeslag = $this->fillVo($aData, $oHuurtoeslag);
		} else {
		     $oHuurtoeslag = new Huurtoeslag();
		     if (!empty($aData)) {
		         $oHuurtoeslag = $this->fillVo($aData, $oHuurtoeslag);
		     }
		}
		return $oHuurtoeslag;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Huurtoeslag
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Huurtoeslag
	{
		$oHuurtoeslag = $this->getModel($aData);


		 if(!empty($oHuurtoeslag))
		 {
		     $oHuurtoeslag = $this->fillVo($aData, $oHuurtoeslag);
		     $oHuurtoeslag->save();
		 }
		return $oHuurtoeslag;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Huurtoeslag $oModel
	 * @return Huurtoeslag
	 */
	protected function fillVo(array $aData, Huurtoeslag $oModel): Huurtoeslag
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['huurtoeslag']) ? $oModel->setHuurtoeslag($aData['huurtoeslag']) : null;
		isset($aData['start_datum']) ? $oModel->setStartDatum($aData['start_datum']) : null;
		return $oModel;
	}
}
