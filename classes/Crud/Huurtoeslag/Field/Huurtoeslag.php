<?php
namespace Crud\Custom\NovumBri\Huurtoeslag\Field;

use Crud\Custom\NovumBri\Huurtoeslag\Field\Base\Huurtoeslag as BaseHuurtoeslag;

/**
 * Skeleton subclass for representing huurtoeslag field from the huurtoeslag table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Huurtoeslag extends BaseHuurtoeslag
{
}
