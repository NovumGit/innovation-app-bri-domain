<?php 
namespace Crud\Custom\NovumBri\Huurtoeslag\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumBri\Huurtoeslag;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Huurtoeslag)
		{
		     return "/custom/novumbri/toeslagen/huurtoeslag/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Huurtoeslag)
		{
		     return "/custom/novumbri/huurtoeslag?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
