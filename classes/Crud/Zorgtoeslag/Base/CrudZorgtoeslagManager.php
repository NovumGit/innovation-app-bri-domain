<?php
namespace Crud\Custom\NovumBri\Zorgtoeslag\Base;

use Crud\Custom\NovumBri;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumBri\Map\ZorgtoeslagTableMap;
use Model\Custom\NovumBri\Zorgtoeslag;
use Model\Custom\NovumBri\ZorgtoeslagQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Zorgtoeslag instead if you need to override or add functionality.
 */
abstract class CrudZorgtoeslagManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumBri\CrudTrait;
	use NovumBri\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return ZorgtoeslagQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumBri\Map\ZorgtoeslagTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint bevat fake zorgtoeslag gegevens.";
	}


	public function getEntityTitle(): string
	{
		return "Zorgtoeslag";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumbri/toeslagen/zorgtoeslag/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumbri/toeslagen/zorgtoeslag/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Zorgtoeslag toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Zorgtoeslag aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Zorgtoeslag', 'StartDatum', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Zorgtoeslag', 'StartDatum'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Zorgtoeslag
	 */
	public function getModel(array $aData = null): Zorgtoeslag
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oZorgtoeslagQuery = ZorgtoeslagQuery::create();
		     $oZorgtoeslag = $oZorgtoeslagQuery->findOneById($aData['id']);
		     if (!$oZorgtoeslag instanceof Zorgtoeslag) {
		         throw new LogicException("Zorgtoeslag should be an instance of Zorgtoeslag but got something else." . __METHOD__);
		     }
		     $oZorgtoeslag = $this->fillVo($aData, $oZorgtoeslag);
		} else {
		     $oZorgtoeslag = new Zorgtoeslag();
		     if (!empty($aData)) {
		         $oZorgtoeslag = $this->fillVo($aData, $oZorgtoeslag);
		     }
		}
		return $oZorgtoeslag;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Zorgtoeslag
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Zorgtoeslag
	{
		$oZorgtoeslag = $this->getModel($aData);


		 if(!empty($oZorgtoeslag))
		 {
		     $oZorgtoeslag = $this->fillVo($aData, $oZorgtoeslag);
		     $oZorgtoeslag->save();
		 }
		return $oZorgtoeslag;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Zorgtoeslag $oModel
	 * @return Zorgtoeslag
	 */
	protected function fillVo(array $aData, Zorgtoeslag $oModel): Zorgtoeslag
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['zorgtoeslag']) ? $oModel->setZorgtoeslag($aData['zorgtoeslag']) : null;
		isset($aData['start_datum']) ? $oModel->setStartDatum($aData['start_datum']) : null;
		return $oModel;
	}
}
