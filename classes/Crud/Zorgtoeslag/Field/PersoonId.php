<?php
namespace Crud\Custom\NovumBri\Zorgtoeslag\Field;

use Crud\Custom\NovumBri\Zorgtoeslag\Field\Base\PersoonId as BasePersoonId;

/**
 * Skeleton subclass for representing persoon_id field from the zorgtoeslag table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class PersoonId extends BasePersoonId
{
}
