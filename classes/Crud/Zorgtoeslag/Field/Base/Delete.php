<?php 
namespace Crud\Custom\NovumBri\Zorgtoeslag\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumBri\Zorgtoeslag;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Zorgtoeslag)
		{
		     return "/custom/novumbri/toeslagen/zorgtoeslag/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Zorgtoeslag)
		{
		     return "/custom/novumbri/zorgtoeslag?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
