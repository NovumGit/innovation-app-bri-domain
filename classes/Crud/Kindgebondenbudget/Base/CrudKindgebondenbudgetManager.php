<?php
namespace Crud\Custom\NovumBri\Kindgebondenbudget\Base;

use Crud\Custom\NovumBri;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumBri\Kindgebondenbudget;
use Model\Custom\NovumBri\KindgebondenbudgetQuery;
use Model\Custom\NovumBri\Map\KindgebondenbudgetTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Kindgebondenbudget instead if you need to override or add functionality.
 */
abstract class CrudKindgebondenbudgetManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumBri\CrudTrait;
	use NovumBri\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return KindgebondenbudgetQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumBri\Map\KindgebondenbudgetTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint bevat fake huurtoeslag gegevens.";
	}


	public function getEntityTitle(): string
	{
		return "Kindgebondenbudget";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumbri/toeslagen/kindgebonden_budget/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumbri/toeslagen/kindgebonden_budget/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Kindgebonden budget toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Kindgebonden budget aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Budget', 'StartDatum', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Budget', 'StartDatum'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Kindgebondenbudget
	 */
	public function getModel(array $aData = null): Kindgebondenbudget
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oKindgebondenbudgetQuery = KindgebondenbudgetQuery::create();
		     $oKindgebondenbudget = $oKindgebondenbudgetQuery->findOneById($aData['id']);
		     if (!$oKindgebondenbudget instanceof Kindgebondenbudget) {
		         throw new LogicException("Kindgebondenbudget should be an instance of Kindgebondenbudget but got something else." . __METHOD__);
		     }
		     $oKindgebondenbudget = $this->fillVo($aData, $oKindgebondenbudget);
		} else {
		     $oKindgebondenbudget = new Kindgebondenbudget();
		     if (!empty($aData)) {
		         $oKindgebondenbudget = $this->fillVo($aData, $oKindgebondenbudget);
		     }
		}
		return $oKindgebondenbudget;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Kindgebondenbudget
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Kindgebondenbudget
	{
		$oKindgebondenbudget = $this->getModel($aData);


		 if(!empty($oKindgebondenbudget))
		 {
		     $oKindgebondenbudget = $this->fillVo($aData, $oKindgebondenbudget);
		     $oKindgebondenbudget->save();
		 }
		return $oKindgebondenbudget;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Kindgebondenbudget $oModel
	 * @return Kindgebondenbudget
	 */
	protected function fillVo(array $aData, Kindgebondenbudget $oModel): Kindgebondenbudget
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['budget']) ? $oModel->setBudget($aData['budget']) : null;
		isset($aData['start_datum']) ? $oModel->setStartDatum($aData['start_datum']) : null;
		return $oModel;
	}
}
