<?php
namespace Crud\Custom\NovumBri\Kindgebondenbudget\Field\Base;

use Crud\Generic\Field\GenericMoney;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'budget' crud field from the 'kindgebonden_budget' table.
 * This class is auto generated and should not be modified.
 */
abstract class Budget extends GenericMoney implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'budget';

	protected $sFieldLabel = 'Huurtoeslag';

	protected $sIcon = 'money';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getBudget';

	protected $sFqModelClassname = '\Model\Custom\NovumBri\Kindgebondenbudget';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['budget']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Huurtoeslag" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
