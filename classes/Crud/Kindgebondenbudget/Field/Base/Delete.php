<?php 
namespace Crud\Custom\NovumBri\Kindgebondenbudget\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumBri\Kindgebondenbudget;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Kindgebondenbudget)
		{
		     return "/custom/novumbri/toeslagen/kindgebonden_budget/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Kindgebondenbudget)
		{
		     return "/custom/novumbri/kindgebonden_budget?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
