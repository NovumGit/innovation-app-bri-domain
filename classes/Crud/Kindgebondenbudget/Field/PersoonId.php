<?php
namespace Crud\Custom\NovumBri\Kindgebondenbudget\Field;

use Crud\Custom\NovumBri\Kindgebondenbudget\Field\Base\PersoonId as BasePersoonId;

/**
 * Skeleton subclass for representing persoon_id field from the kindgebonden_budget table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class PersoonId extends BasePersoonId
{
}
