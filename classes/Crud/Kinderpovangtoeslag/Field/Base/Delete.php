<?php 
namespace Crud\Custom\NovumBri\Kinderpovangtoeslag\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumBri\Kinderpovangtoeslag;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Kinderpovangtoeslag)
		{
		     return "/custom/novumbri/toeslagen/kinderpovangtoeslag/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Kinderpovangtoeslag)
		{
		     return "/custom/novumbri/kinderpovangtoeslag?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
