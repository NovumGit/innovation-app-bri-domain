<?php
namespace Crud\Custom\NovumBri\Kinderpovangtoeslag\Field\Base;

use Crud\Generic\Field\GenericMoney;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'kinderpovangtoeslag' crud field from the 'kinderpovangtoeslag' table.
 * This class is auto generated and should not be modified.
 */
abstract class Kinderpovangtoeslag extends GenericMoney implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'kinderpovangtoeslag';

	protected $sFieldLabel = 'Kinderpovangtoeslag';

	protected $sIcon = 'money';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getKinderpovangtoeslag';

	protected $sFqModelClassname = '\Model\Custom\NovumBri\Kinderpovangtoeslag';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['kinderpovangtoeslag']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Kinderpovangtoeslag" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
