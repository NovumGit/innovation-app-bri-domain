<?php
namespace Crud\Custom\NovumBri\Kinderpovangtoeslag\Base;

use Crud\Custom\NovumBri;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumBri\Kinderpovangtoeslag;
use Model\Custom\NovumBri\KinderpovangtoeslagQuery;
use Model\Custom\NovumBri\Map\KinderpovangtoeslagTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Kinderpovangtoeslag instead if you need to override or add functionality.
 */
abstract class CrudKinderpovangtoeslagManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumBri\CrudTrait;
	use NovumBri\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return KinderpovangtoeslagQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumBri\Map\KinderpovangtoeslagTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint bevat fake kinderpovangtoeslag gegevens.";
	}


	public function getEntityTitle(): string
	{
		return "Kinderpovangtoeslag";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumbri/toeslagen/kinderpovangtoeslag/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumbri/toeslagen/kinderpovangtoeslag/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Kinderopvangtoeslag toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Kinderopvangtoeslag aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Kinderpovangtoeslag', 'StartDatum', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Kinderpovangtoeslag', 'StartDatum'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Kinderpovangtoeslag
	 */
	public function getModel(array $aData = null): Kinderpovangtoeslag
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oKinderpovangtoeslagQuery = KinderpovangtoeslagQuery::create();
		     $oKinderpovangtoeslag = $oKinderpovangtoeslagQuery->findOneById($aData['id']);
		     if (!$oKinderpovangtoeslag instanceof Kinderpovangtoeslag) {
		         throw new LogicException("Kinderpovangtoeslag should be an instance of Kinderpovangtoeslag but got something else." . __METHOD__);
		     }
		     $oKinderpovangtoeslag = $this->fillVo($aData, $oKinderpovangtoeslag);
		} else {
		     $oKinderpovangtoeslag = new Kinderpovangtoeslag();
		     if (!empty($aData)) {
		         $oKinderpovangtoeslag = $this->fillVo($aData, $oKinderpovangtoeslag);
		     }
		}
		return $oKinderpovangtoeslag;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Kinderpovangtoeslag
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Kinderpovangtoeslag
	{
		$oKinderpovangtoeslag = $this->getModel($aData);


		 if(!empty($oKinderpovangtoeslag))
		 {
		     $oKinderpovangtoeslag = $this->fillVo($aData, $oKinderpovangtoeslag);
		     $oKinderpovangtoeslag->save();
		 }
		return $oKinderpovangtoeslag;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Kinderpovangtoeslag $oModel
	 * @return Kinderpovangtoeslag
	 */
	protected function fillVo(array $aData, Kinderpovangtoeslag $oModel): Kinderpovangtoeslag
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['kinderpovangtoeslag']) ? $oModel->setKinderpovangtoeslag($aData['kinderpovangtoeslag']) : null;
		isset($aData['start_datum']) ? $oModel->setStartDatum($aData['start_datum']) : null;
		return $oModel;
	}
}
