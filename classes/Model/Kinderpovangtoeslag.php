<?php

namespace Model\Custom\NovumBri;

use Model\Custom\NovumBri\Base\Kinderpovangtoeslag as BaseKinderpovangtoeslag;

/**
 * Skeleton subclass for representing a row from the 'kinderpovangtoeslag' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Kinderpovangtoeslag extends BaseKinderpovangtoeslag
{

}
