<?php

namespace Model\Custom\NovumBri\Base;

use \Exception;
use \PDO;
use Model\Custom\NovumBri\Huurtoeslag as ChildHuurtoeslag;
use Model\Custom\NovumBri\HuurtoeslagQuery as ChildHuurtoeslagQuery;
use Model\Custom\NovumBri\Inkomen as ChildInkomen;
use Model\Custom\NovumBri\InkomenQuery as ChildInkomenQuery;
use Model\Custom\NovumBri\Kinderpovangtoeslag as ChildKinderpovangtoeslag;
use Model\Custom\NovumBri\KinderpovangtoeslagQuery as ChildKinderpovangtoeslagQuery;
use Model\Custom\NovumBri\Kindgebondenbudget as ChildKindgebondenbudget;
use Model\Custom\NovumBri\KindgebondenbudgetQuery as ChildKindgebondenbudgetQuery;
use Model\Custom\NovumBri\Persoon as ChildPersoon;
use Model\Custom\NovumBri\PersoonQuery as ChildPersoonQuery;
use Model\Custom\NovumBri\Zorgtoeslag as ChildZorgtoeslag;
use Model\Custom\NovumBri\ZorgtoeslagQuery as ChildZorgtoeslagQuery;
use Model\Custom\NovumBri\Map\HuurtoeslagTableMap;
use Model\Custom\NovumBri\Map\InkomenTableMap;
use Model\Custom\NovumBri\Map\KinderpovangtoeslagTableMap;
use Model\Custom\NovumBri\Map\KindgebondenbudgetTableMap;
use Model\Custom\NovumBri\Map\PersoonTableMap;
use Model\Custom\NovumBri\Map\ZorgtoeslagTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'persoon' table.
 *
 *
 *
 * @package    propel.generator.Model.Custom.NovumBri.Base
 */
abstract class Persoon implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Custom\\NovumBri\\Map\\PersoonTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the bsn field.
     *
     * @var        string
     */
    protected $bsn;

    /**
     * The value for the voornaam field.
     *
     * @var        string
     */
    protected $voornaam;

    /**
     * @var        ObjectCollection|ChildHuurtoeslag[] Collection to store aggregation of ChildHuurtoeslag objects.
     */
    protected $collHuurtoeslags;
    protected $collHuurtoeslagsPartial;

    /**
     * @var        ObjectCollection|ChildKindgebondenbudget[] Collection to store aggregation of ChildKindgebondenbudget objects.
     */
    protected $collKindgebondenbudgets;
    protected $collKindgebondenbudgetsPartial;

    /**
     * @var        ObjectCollection|ChildZorgtoeslag[] Collection to store aggregation of ChildZorgtoeslag objects.
     */
    protected $collZorgtoeslags;
    protected $collZorgtoeslagsPartial;

    /**
     * @var        ObjectCollection|ChildKinderpovangtoeslag[] Collection to store aggregation of ChildKinderpovangtoeslag objects.
     */
    protected $collKinderpovangtoeslags;
    protected $collKinderpovangtoeslagsPartial;

    /**
     * @var        ObjectCollection|ChildInkomen[] Collection to store aggregation of ChildInkomen objects.
     */
    protected $collInkomens;
    protected $collInkomensPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildHuurtoeslag[]
     */
    protected $huurtoeslagsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildKindgebondenbudget[]
     */
    protected $kindgebondenbudgetsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildZorgtoeslag[]
     */
    protected $zorgtoeslagsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildKinderpovangtoeslag[]
     */
    protected $kinderpovangtoeslagsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildInkomen[]
     */
    protected $inkomensScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\Custom\NovumBri\Base\Persoon object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Persoon</code> instance.  If
     * <code>obj</code> is an instance of <code>Persoon</code>, delegates to
     * <code>equals(Persoon)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Persoon The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [bsn] column value.
     *
     * @return string
     */
    public function getBsn()
    {
        return $this->bsn;
    }

    /**
     * Get the [voornaam] column value.
     *
     * @return string
     */
    public function getVoornaam()
    {
        return $this->voornaam;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumBri\Persoon The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[PersoonTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [bsn] column.
     *
     * @param string $v new value
     * @return $this|\Model\Custom\NovumBri\Persoon The current object (for fluent API support)
     */
    public function setBsn($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bsn !== $v) {
            $this->bsn = $v;
            $this->modifiedColumns[PersoonTableMap::COL_BSN] = true;
        }

        return $this;
    } // setBsn()

    /**
     * Set the value of [voornaam] column.
     *
     * @param string $v new value
     * @return $this|\Model\Custom\NovumBri\Persoon The current object (for fluent API support)
     */
    public function setVoornaam($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->voornaam !== $v) {
            $this->voornaam = $v;
            $this->modifiedColumns[PersoonTableMap::COL_VOORNAAM] = true;
        }

        return $this;
    } // setVoornaam()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PersoonTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PersoonTableMap::translateFieldName('Bsn', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bsn = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PersoonTableMap::translateFieldName('Voornaam', TableMap::TYPE_PHPNAME, $indexType)];
            $this->voornaam = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 3; // 3 = PersoonTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Custom\\NovumBri\\Persoon'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PersoonTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPersoonQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collHuurtoeslags = null;

            $this->collKindgebondenbudgets = null;

            $this->collZorgtoeslags = null;

            $this->collKinderpovangtoeslags = null;

            $this->collInkomens = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Persoon::setDeleted()
     * @see Persoon::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PersoonTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPersoonQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PersoonTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PersoonTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->huurtoeslagsScheduledForDeletion !== null) {
                if (!$this->huurtoeslagsScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumBri\HuurtoeslagQuery::create()
                        ->filterByPrimaryKeys($this->huurtoeslagsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->huurtoeslagsScheduledForDeletion = null;
                }
            }

            if ($this->collHuurtoeslags !== null) {
                foreach ($this->collHuurtoeslags as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->kindgebondenbudgetsScheduledForDeletion !== null) {
                if (!$this->kindgebondenbudgetsScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumBri\KindgebondenbudgetQuery::create()
                        ->filterByPrimaryKeys($this->kindgebondenbudgetsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->kindgebondenbudgetsScheduledForDeletion = null;
                }
            }

            if ($this->collKindgebondenbudgets !== null) {
                foreach ($this->collKindgebondenbudgets as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->zorgtoeslagsScheduledForDeletion !== null) {
                if (!$this->zorgtoeslagsScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumBri\ZorgtoeslagQuery::create()
                        ->filterByPrimaryKeys($this->zorgtoeslagsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->zorgtoeslagsScheduledForDeletion = null;
                }
            }

            if ($this->collZorgtoeslags !== null) {
                foreach ($this->collZorgtoeslags as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->kinderpovangtoeslagsScheduledForDeletion !== null) {
                if (!$this->kinderpovangtoeslagsScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumBri\KinderpovangtoeslagQuery::create()
                        ->filterByPrimaryKeys($this->kinderpovangtoeslagsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->kinderpovangtoeslagsScheduledForDeletion = null;
                }
            }

            if ($this->collKinderpovangtoeslags !== null) {
                foreach ($this->collKinderpovangtoeslags as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->inkomensScheduledForDeletion !== null) {
                if (!$this->inkomensScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumBri\InkomenQuery::create()
                        ->filterByPrimaryKeys($this->inkomensScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->inkomensScheduledForDeletion = null;
                }
            }

            if ($this->collInkomens !== null) {
                foreach ($this->collInkomens as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PersoonTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PersoonTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PersoonTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(PersoonTableMap::COL_BSN)) {
            $modifiedColumns[':p' . $index++]  = 'bsn';
        }
        if ($this->isColumnModified(PersoonTableMap::COL_VOORNAAM)) {
            $modifiedColumns[':p' . $index++]  = 'voornaam';
        }

        $sql = sprintf(
            'INSERT INTO persoon (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'bsn':
                        $stmt->bindValue($identifier, $this->bsn, PDO::PARAM_STR);
                        break;
                    case 'voornaam':
                        $stmt->bindValue($identifier, $this->voornaam, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PersoonTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getBsn();
                break;
            case 2:
                return $this->getVoornaam();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Persoon'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Persoon'][$this->hashCode()] = true;
        $keys = PersoonTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getBsn(),
            $keys[2] => $this->getVoornaam(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collHuurtoeslags) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'huurtoeslags';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'huurtoeslags';
                        break;
                    default:
                        $key = 'Huurtoeslags';
                }

                $result[$key] = $this->collHuurtoeslags->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collKindgebondenbudgets) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'kindgebondenbudgets';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'kindgebonden_budgets';
                        break;
                    default:
                        $key = 'Kindgebondenbudgets';
                }

                $result[$key] = $this->collKindgebondenbudgets->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collZorgtoeslags) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'zorgtoeslags';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'zorgtoeslags';
                        break;
                    default:
                        $key = 'Zorgtoeslags';
                }

                $result[$key] = $this->collZorgtoeslags->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collKinderpovangtoeslags) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'kinderpovangtoeslags';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'kinderpovangtoeslags';
                        break;
                    default:
                        $key = 'Kinderpovangtoeslags';
                }

                $result[$key] = $this->collKinderpovangtoeslags->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collInkomens) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'inkomens';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'inkomens';
                        break;
                    default:
                        $key = 'Inkomens';
                }

                $result[$key] = $this->collInkomens->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Custom\NovumBri\Persoon
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PersoonTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Custom\NovumBri\Persoon
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setBsn($value);
                break;
            case 2:
                $this->setVoornaam($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PersoonTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBsn($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setVoornaam($arr[$keys[2]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Custom\NovumBri\Persoon The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PersoonTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PersoonTableMap::COL_ID)) {
            $criteria->add(PersoonTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(PersoonTableMap::COL_BSN)) {
            $criteria->add(PersoonTableMap::COL_BSN, $this->bsn);
        }
        if ($this->isColumnModified(PersoonTableMap::COL_VOORNAAM)) {
            $criteria->add(PersoonTableMap::COL_VOORNAAM, $this->voornaam);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPersoonQuery::create();
        $criteria->add(PersoonTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Custom\NovumBri\Persoon (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBsn($this->getBsn());
        $copyObj->setVoornaam($this->getVoornaam());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getHuurtoeslags() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addHuurtoeslag($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getKindgebondenbudgets() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addKindgebondenbudget($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getZorgtoeslags() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addZorgtoeslag($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getKinderpovangtoeslags() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addKinderpovangtoeslag($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getInkomens() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInkomen($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Custom\NovumBri\Persoon Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Huurtoeslag' == $relationName) {
            $this->initHuurtoeslags();
            return;
        }
        if ('Kindgebondenbudget' == $relationName) {
            $this->initKindgebondenbudgets();
            return;
        }
        if ('Zorgtoeslag' == $relationName) {
            $this->initZorgtoeslags();
            return;
        }
        if ('Kinderpovangtoeslag' == $relationName) {
            $this->initKinderpovangtoeslags();
            return;
        }
        if ('Inkomen' == $relationName) {
            $this->initInkomens();
            return;
        }
    }

    /**
     * Clears out the collHuurtoeslags collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addHuurtoeslags()
     */
    public function clearHuurtoeslags()
    {
        $this->collHuurtoeslags = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collHuurtoeslags collection loaded partially.
     */
    public function resetPartialHuurtoeslags($v = true)
    {
        $this->collHuurtoeslagsPartial = $v;
    }

    /**
     * Initializes the collHuurtoeslags collection.
     *
     * By default this just sets the collHuurtoeslags collection to an empty array (like clearcollHuurtoeslags());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initHuurtoeslags($overrideExisting = true)
    {
        if (null !== $this->collHuurtoeslags && !$overrideExisting) {
            return;
        }

        $collectionClassName = HuurtoeslagTableMap::getTableMap()->getCollectionClassName();

        $this->collHuurtoeslags = new $collectionClassName;
        $this->collHuurtoeslags->setModel('\Model\Custom\NovumBri\Huurtoeslag');
    }

    /**
     * Gets an array of ChildHuurtoeslag objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildHuurtoeslag[] List of ChildHuurtoeslag objects
     * @throws PropelException
     */
    public function getHuurtoeslags(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collHuurtoeslagsPartial && !$this->isNew();
        if (null === $this->collHuurtoeslags || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collHuurtoeslags) {
                // return empty collection
                $this->initHuurtoeslags();
            } else {
                $collHuurtoeslags = ChildHuurtoeslagQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collHuurtoeslagsPartial && count($collHuurtoeslags)) {
                        $this->initHuurtoeslags(false);

                        foreach ($collHuurtoeslags as $obj) {
                            if (false == $this->collHuurtoeslags->contains($obj)) {
                                $this->collHuurtoeslags->append($obj);
                            }
                        }

                        $this->collHuurtoeslagsPartial = true;
                    }

                    return $collHuurtoeslags;
                }

                if ($partial && $this->collHuurtoeslags) {
                    foreach ($this->collHuurtoeslags as $obj) {
                        if ($obj->isNew()) {
                            $collHuurtoeslags[] = $obj;
                        }
                    }
                }

                $this->collHuurtoeslags = $collHuurtoeslags;
                $this->collHuurtoeslagsPartial = false;
            }
        }

        return $this->collHuurtoeslags;
    }

    /**
     * Sets a collection of ChildHuurtoeslag objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $huurtoeslags A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setHuurtoeslags(Collection $huurtoeslags, ConnectionInterface $con = null)
    {
        /** @var ChildHuurtoeslag[] $huurtoeslagsToDelete */
        $huurtoeslagsToDelete = $this->getHuurtoeslags(new Criteria(), $con)->diff($huurtoeslags);


        $this->huurtoeslagsScheduledForDeletion = $huurtoeslagsToDelete;

        foreach ($huurtoeslagsToDelete as $huurtoeslagRemoved) {
            $huurtoeslagRemoved->setPersoon(null);
        }

        $this->collHuurtoeslags = null;
        foreach ($huurtoeslags as $huurtoeslag) {
            $this->addHuurtoeslag($huurtoeslag);
        }

        $this->collHuurtoeslags = $huurtoeslags;
        $this->collHuurtoeslagsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Huurtoeslag objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Huurtoeslag objects.
     * @throws PropelException
     */
    public function countHuurtoeslags(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collHuurtoeslagsPartial && !$this->isNew();
        if (null === $this->collHuurtoeslags || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collHuurtoeslags) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getHuurtoeslags());
            }

            $query = ChildHuurtoeslagQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collHuurtoeslags);
    }

    /**
     * Method called to associate a ChildHuurtoeslag object to this object
     * through the ChildHuurtoeslag foreign key attribute.
     *
     * @param  ChildHuurtoeslag $l ChildHuurtoeslag
     * @return $this|\Model\Custom\NovumBri\Persoon The current object (for fluent API support)
     */
    public function addHuurtoeslag(ChildHuurtoeslag $l)
    {
        if ($this->collHuurtoeslags === null) {
            $this->initHuurtoeslags();
            $this->collHuurtoeslagsPartial = true;
        }

        if (!$this->collHuurtoeslags->contains($l)) {
            $this->doAddHuurtoeslag($l);

            if ($this->huurtoeslagsScheduledForDeletion and $this->huurtoeslagsScheduledForDeletion->contains($l)) {
                $this->huurtoeslagsScheduledForDeletion->remove($this->huurtoeslagsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildHuurtoeslag $huurtoeslag The ChildHuurtoeslag object to add.
     */
    protected function doAddHuurtoeslag(ChildHuurtoeslag $huurtoeslag)
    {
        $this->collHuurtoeslags[]= $huurtoeslag;
        $huurtoeslag->setPersoon($this);
    }

    /**
     * @param  ChildHuurtoeslag $huurtoeslag The ChildHuurtoeslag object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removeHuurtoeslag(ChildHuurtoeslag $huurtoeslag)
    {
        if ($this->getHuurtoeslags()->contains($huurtoeslag)) {
            $pos = $this->collHuurtoeslags->search($huurtoeslag);
            $this->collHuurtoeslags->remove($pos);
            if (null === $this->huurtoeslagsScheduledForDeletion) {
                $this->huurtoeslagsScheduledForDeletion = clone $this->collHuurtoeslags;
                $this->huurtoeslagsScheduledForDeletion->clear();
            }
            $this->huurtoeslagsScheduledForDeletion[]= clone $huurtoeslag;
            $huurtoeslag->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears out the collKindgebondenbudgets collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addKindgebondenbudgets()
     */
    public function clearKindgebondenbudgets()
    {
        $this->collKindgebondenbudgets = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collKindgebondenbudgets collection loaded partially.
     */
    public function resetPartialKindgebondenbudgets($v = true)
    {
        $this->collKindgebondenbudgetsPartial = $v;
    }

    /**
     * Initializes the collKindgebondenbudgets collection.
     *
     * By default this just sets the collKindgebondenbudgets collection to an empty array (like clearcollKindgebondenbudgets());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initKindgebondenbudgets($overrideExisting = true)
    {
        if (null !== $this->collKindgebondenbudgets && !$overrideExisting) {
            return;
        }

        $collectionClassName = KindgebondenbudgetTableMap::getTableMap()->getCollectionClassName();

        $this->collKindgebondenbudgets = new $collectionClassName;
        $this->collKindgebondenbudgets->setModel('\Model\Custom\NovumBri\Kindgebondenbudget');
    }

    /**
     * Gets an array of ChildKindgebondenbudget objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildKindgebondenbudget[] List of ChildKindgebondenbudget objects
     * @throws PropelException
     */
    public function getKindgebondenbudgets(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collKindgebondenbudgetsPartial && !$this->isNew();
        if (null === $this->collKindgebondenbudgets || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collKindgebondenbudgets) {
                // return empty collection
                $this->initKindgebondenbudgets();
            } else {
                $collKindgebondenbudgets = ChildKindgebondenbudgetQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collKindgebondenbudgetsPartial && count($collKindgebondenbudgets)) {
                        $this->initKindgebondenbudgets(false);

                        foreach ($collKindgebondenbudgets as $obj) {
                            if (false == $this->collKindgebondenbudgets->contains($obj)) {
                                $this->collKindgebondenbudgets->append($obj);
                            }
                        }

                        $this->collKindgebondenbudgetsPartial = true;
                    }

                    return $collKindgebondenbudgets;
                }

                if ($partial && $this->collKindgebondenbudgets) {
                    foreach ($this->collKindgebondenbudgets as $obj) {
                        if ($obj->isNew()) {
                            $collKindgebondenbudgets[] = $obj;
                        }
                    }
                }

                $this->collKindgebondenbudgets = $collKindgebondenbudgets;
                $this->collKindgebondenbudgetsPartial = false;
            }
        }

        return $this->collKindgebondenbudgets;
    }

    /**
     * Sets a collection of ChildKindgebondenbudget objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $kindgebondenbudgets A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setKindgebondenbudgets(Collection $kindgebondenbudgets, ConnectionInterface $con = null)
    {
        /** @var ChildKindgebondenbudget[] $kindgebondenbudgetsToDelete */
        $kindgebondenbudgetsToDelete = $this->getKindgebondenbudgets(new Criteria(), $con)->diff($kindgebondenbudgets);


        $this->kindgebondenbudgetsScheduledForDeletion = $kindgebondenbudgetsToDelete;

        foreach ($kindgebondenbudgetsToDelete as $kindgebondenbudgetRemoved) {
            $kindgebondenbudgetRemoved->setPersoon(null);
        }

        $this->collKindgebondenbudgets = null;
        foreach ($kindgebondenbudgets as $kindgebondenbudget) {
            $this->addKindgebondenbudget($kindgebondenbudget);
        }

        $this->collKindgebondenbudgets = $kindgebondenbudgets;
        $this->collKindgebondenbudgetsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Kindgebondenbudget objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Kindgebondenbudget objects.
     * @throws PropelException
     */
    public function countKindgebondenbudgets(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collKindgebondenbudgetsPartial && !$this->isNew();
        if (null === $this->collKindgebondenbudgets || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collKindgebondenbudgets) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getKindgebondenbudgets());
            }

            $query = ChildKindgebondenbudgetQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collKindgebondenbudgets);
    }

    /**
     * Method called to associate a ChildKindgebondenbudget object to this object
     * through the ChildKindgebondenbudget foreign key attribute.
     *
     * @param  ChildKindgebondenbudget $l ChildKindgebondenbudget
     * @return $this|\Model\Custom\NovumBri\Persoon The current object (for fluent API support)
     */
    public function addKindgebondenbudget(ChildKindgebondenbudget $l)
    {
        if ($this->collKindgebondenbudgets === null) {
            $this->initKindgebondenbudgets();
            $this->collKindgebondenbudgetsPartial = true;
        }

        if (!$this->collKindgebondenbudgets->contains($l)) {
            $this->doAddKindgebondenbudget($l);

            if ($this->kindgebondenbudgetsScheduledForDeletion and $this->kindgebondenbudgetsScheduledForDeletion->contains($l)) {
                $this->kindgebondenbudgetsScheduledForDeletion->remove($this->kindgebondenbudgetsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildKindgebondenbudget $kindgebondenbudget The ChildKindgebondenbudget object to add.
     */
    protected function doAddKindgebondenbudget(ChildKindgebondenbudget $kindgebondenbudget)
    {
        $this->collKindgebondenbudgets[]= $kindgebondenbudget;
        $kindgebondenbudget->setPersoon($this);
    }

    /**
     * @param  ChildKindgebondenbudget $kindgebondenbudget The ChildKindgebondenbudget object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removeKindgebondenbudget(ChildKindgebondenbudget $kindgebondenbudget)
    {
        if ($this->getKindgebondenbudgets()->contains($kindgebondenbudget)) {
            $pos = $this->collKindgebondenbudgets->search($kindgebondenbudget);
            $this->collKindgebondenbudgets->remove($pos);
            if (null === $this->kindgebondenbudgetsScheduledForDeletion) {
                $this->kindgebondenbudgetsScheduledForDeletion = clone $this->collKindgebondenbudgets;
                $this->kindgebondenbudgetsScheduledForDeletion->clear();
            }
            $this->kindgebondenbudgetsScheduledForDeletion[]= clone $kindgebondenbudget;
            $kindgebondenbudget->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears out the collZorgtoeslags collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addZorgtoeslags()
     */
    public function clearZorgtoeslags()
    {
        $this->collZorgtoeslags = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collZorgtoeslags collection loaded partially.
     */
    public function resetPartialZorgtoeslags($v = true)
    {
        $this->collZorgtoeslagsPartial = $v;
    }

    /**
     * Initializes the collZorgtoeslags collection.
     *
     * By default this just sets the collZorgtoeslags collection to an empty array (like clearcollZorgtoeslags());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initZorgtoeslags($overrideExisting = true)
    {
        if (null !== $this->collZorgtoeslags && !$overrideExisting) {
            return;
        }

        $collectionClassName = ZorgtoeslagTableMap::getTableMap()->getCollectionClassName();

        $this->collZorgtoeslags = new $collectionClassName;
        $this->collZorgtoeslags->setModel('\Model\Custom\NovumBri\Zorgtoeslag');
    }

    /**
     * Gets an array of ChildZorgtoeslag objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildZorgtoeslag[] List of ChildZorgtoeslag objects
     * @throws PropelException
     */
    public function getZorgtoeslags(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collZorgtoeslagsPartial && !$this->isNew();
        if (null === $this->collZorgtoeslags || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collZorgtoeslags) {
                // return empty collection
                $this->initZorgtoeslags();
            } else {
                $collZorgtoeslags = ChildZorgtoeslagQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collZorgtoeslagsPartial && count($collZorgtoeslags)) {
                        $this->initZorgtoeslags(false);

                        foreach ($collZorgtoeslags as $obj) {
                            if (false == $this->collZorgtoeslags->contains($obj)) {
                                $this->collZorgtoeslags->append($obj);
                            }
                        }

                        $this->collZorgtoeslagsPartial = true;
                    }

                    return $collZorgtoeslags;
                }

                if ($partial && $this->collZorgtoeslags) {
                    foreach ($this->collZorgtoeslags as $obj) {
                        if ($obj->isNew()) {
                            $collZorgtoeslags[] = $obj;
                        }
                    }
                }

                $this->collZorgtoeslags = $collZorgtoeslags;
                $this->collZorgtoeslagsPartial = false;
            }
        }

        return $this->collZorgtoeslags;
    }

    /**
     * Sets a collection of ChildZorgtoeslag objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $zorgtoeslags A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setZorgtoeslags(Collection $zorgtoeslags, ConnectionInterface $con = null)
    {
        /** @var ChildZorgtoeslag[] $zorgtoeslagsToDelete */
        $zorgtoeslagsToDelete = $this->getZorgtoeslags(new Criteria(), $con)->diff($zorgtoeslags);


        $this->zorgtoeslagsScheduledForDeletion = $zorgtoeslagsToDelete;

        foreach ($zorgtoeslagsToDelete as $zorgtoeslagRemoved) {
            $zorgtoeslagRemoved->setPersoon(null);
        }

        $this->collZorgtoeslags = null;
        foreach ($zorgtoeslags as $zorgtoeslag) {
            $this->addZorgtoeslag($zorgtoeslag);
        }

        $this->collZorgtoeslags = $zorgtoeslags;
        $this->collZorgtoeslagsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Zorgtoeslag objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Zorgtoeslag objects.
     * @throws PropelException
     */
    public function countZorgtoeslags(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collZorgtoeslagsPartial && !$this->isNew();
        if (null === $this->collZorgtoeslags || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collZorgtoeslags) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getZorgtoeslags());
            }

            $query = ChildZorgtoeslagQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collZorgtoeslags);
    }

    /**
     * Method called to associate a ChildZorgtoeslag object to this object
     * through the ChildZorgtoeslag foreign key attribute.
     *
     * @param  ChildZorgtoeslag $l ChildZorgtoeslag
     * @return $this|\Model\Custom\NovumBri\Persoon The current object (for fluent API support)
     */
    public function addZorgtoeslag(ChildZorgtoeslag $l)
    {
        if ($this->collZorgtoeslags === null) {
            $this->initZorgtoeslags();
            $this->collZorgtoeslagsPartial = true;
        }

        if (!$this->collZorgtoeslags->contains($l)) {
            $this->doAddZorgtoeslag($l);

            if ($this->zorgtoeslagsScheduledForDeletion and $this->zorgtoeslagsScheduledForDeletion->contains($l)) {
                $this->zorgtoeslagsScheduledForDeletion->remove($this->zorgtoeslagsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildZorgtoeslag $zorgtoeslag The ChildZorgtoeslag object to add.
     */
    protected function doAddZorgtoeslag(ChildZorgtoeslag $zorgtoeslag)
    {
        $this->collZorgtoeslags[]= $zorgtoeslag;
        $zorgtoeslag->setPersoon($this);
    }

    /**
     * @param  ChildZorgtoeslag $zorgtoeslag The ChildZorgtoeslag object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removeZorgtoeslag(ChildZorgtoeslag $zorgtoeslag)
    {
        if ($this->getZorgtoeslags()->contains($zorgtoeslag)) {
            $pos = $this->collZorgtoeslags->search($zorgtoeslag);
            $this->collZorgtoeslags->remove($pos);
            if (null === $this->zorgtoeslagsScheduledForDeletion) {
                $this->zorgtoeslagsScheduledForDeletion = clone $this->collZorgtoeslags;
                $this->zorgtoeslagsScheduledForDeletion->clear();
            }
            $this->zorgtoeslagsScheduledForDeletion[]= clone $zorgtoeslag;
            $zorgtoeslag->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears out the collKinderpovangtoeslags collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addKinderpovangtoeslags()
     */
    public function clearKinderpovangtoeslags()
    {
        $this->collKinderpovangtoeslags = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collKinderpovangtoeslags collection loaded partially.
     */
    public function resetPartialKinderpovangtoeslags($v = true)
    {
        $this->collKinderpovangtoeslagsPartial = $v;
    }

    /**
     * Initializes the collKinderpovangtoeslags collection.
     *
     * By default this just sets the collKinderpovangtoeslags collection to an empty array (like clearcollKinderpovangtoeslags());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initKinderpovangtoeslags($overrideExisting = true)
    {
        if (null !== $this->collKinderpovangtoeslags && !$overrideExisting) {
            return;
        }

        $collectionClassName = KinderpovangtoeslagTableMap::getTableMap()->getCollectionClassName();

        $this->collKinderpovangtoeslags = new $collectionClassName;
        $this->collKinderpovangtoeslags->setModel('\Model\Custom\NovumBri\Kinderpovangtoeslag');
    }

    /**
     * Gets an array of ChildKinderpovangtoeslag objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildKinderpovangtoeslag[] List of ChildKinderpovangtoeslag objects
     * @throws PropelException
     */
    public function getKinderpovangtoeslags(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collKinderpovangtoeslagsPartial && !$this->isNew();
        if (null === $this->collKinderpovangtoeslags || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collKinderpovangtoeslags) {
                // return empty collection
                $this->initKinderpovangtoeslags();
            } else {
                $collKinderpovangtoeslags = ChildKinderpovangtoeslagQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collKinderpovangtoeslagsPartial && count($collKinderpovangtoeslags)) {
                        $this->initKinderpovangtoeslags(false);

                        foreach ($collKinderpovangtoeslags as $obj) {
                            if (false == $this->collKinderpovangtoeslags->contains($obj)) {
                                $this->collKinderpovangtoeslags->append($obj);
                            }
                        }

                        $this->collKinderpovangtoeslagsPartial = true;
                    }

                    return $collKinderpovangtoeslags;
                }

                if ($partial && $this->collKinderpovangtoeslags) {
                    foreach ($this->collKinderpovangtoeslags as $obj) {
                        if ($obj->isNew()) {
                            $collKinderpovangtoeslags[] = $obj;
                        }
                    }
                }

                $this->collKinderpovangtoeslags = $collKinderpovangtoeslags;
                $this->collKinderpovangtoeslagsPartial = false;
            }
        }

        return $this->collKinderpovangtoeslags;
    }

    /**
     * Sets a collection of ChildKinderpovangtoeslag objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $kinderpovangtoeslags A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setKinderpovangtoeslags(Collection $kinderpovangtoeslags, ConnectionInterface $con = null)
    {
        /** @var ChildKinderpovangtoeslag[] $kinderpovangtoeslagsToDelete */
        $kinderpovangtoeslagsToDelete = $this->getKinderpovangtoeslags(new Criteria(), $con)->diff($kinderpovangtoeslags);


        $this->kinderpovangtoeslagsScheduledForDeletion = $kinderpovangtoeslagsToDelete;

        foreach ($kinderpovangtoeslagsToDelete as $kinderpovangtoeslagRemoved) {
            $kinderpovangtoeslagRemoved->setPersoon(null);
        }

        $this->collKinderpovangtoeslags = null;
        foreach ($kinderpovangtoeslags as $kinderpovangtoeslag) {
            $this->addKinderpovangtoeslag($kinderpovangtoeslag);
        }

        $this->collKinderpovangtoeslags = $kinderpovangtoeslags;
        $this->collKinderpovangtoeslagsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Kinderpovangtoeslag objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Kinderpovangtoeslag objects.
     * @throws PropelException
     */
    public function countKinderpovangtoeslags(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collKinderpovangtoeslagsPartial && !$this->isNew();
        if (null === $this->collKinderpovangtoeslags || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collKinderpovangtoeslags) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getKinderpovangtoeslags());
            }

            $query = ChildKinderpovangtoeslagQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collKinderpovangtoeslags);
    }

    /**
     * Method called to associate a ChildKinderpovangtoeslag object to this object
     * through the ChildKinderpovangtoeslag foreign key attribute.
     *
     * @param  ChildKinderpovangtoeslag $l ChildKinderpovangtoeslag
     * @return $this|\Model\Custom\NovumBri\Persoon The current object (for fluent API support)
     */
    public function addKinderpovangtoeslag(ChildKinderpovangtoeslag $l)
    {
        if ($this->collKinderpovangtoeslags === null) {
            $this->initKinderpovangtoeslags();
            $this->collKinderpovangtoeslagsPartial = true;
        }

        if (!$this->collKinderpovangtoeslags->contains($l)) {
            $this->doAddKinderpovangtoeslag($l);

            if ($this->kinderpovangtoeslagsScheduledForDeletion and $this->kinderpovangtoeslagsScheduledForDeletion->contains($l)) {
                $this->kinderpovangtoeslagsScheduledForDeletion->remove($this->kinderpovangtoeslagsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildKinderpovangtoeslag $kinderpovangtoeslag The ChildKinderpovangtoeslag object to add.
     */
    protected function doAddKinderpovangtoeslag(ChildKinderpovangtoeslag $kinderpovangtoeslag)
    {
        $this->collKinderpovangtoeslags[]= $kinderpovangtoeslag;
        $kinderpovangtoeslag->setPersoon($this);
    }

    /**
     * @param  ChildKinderpovangtoeslag $kinderpovangtoeslag The ChildKinderpovangtoeslag object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removeKinderpovangtoeslag(ChildKinderpovangtoeslag $kinderpovangtoeslag)
    {
        if ($this->getKinderpovangtoeslags()->contains($kinderpovangtoeslag)) {
            $pos = $this->collKinderpovangtoeslags->search($kinderpovangtoeslag);
            $this->collKinderpovangtoeslags->remove($pos);
            if (null === $this->kinderpovangtoeslagsScheduledForDeletion) {
                $this->kinderpovangtoeslagsScheduledForDeletion = clone $this->collKinderpovangtoeslags;
                $this->kinderpovangtoeslagsScheduledForDeletion->clear();
            }
            $this->kinderpovangtoeslagsScheduledForDeletion[]= clone $kinderpovangtoeslag;
            $kinderpovangtoeslag->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears out the collInkomens collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addInkomens()
     */
    public function clearInkomens()
    {
        $this->collInkomens = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collInkomens collection loaded partially.
     */
    public function resetPartialInkomens($v = true)
    {
        $this->collInkomensPartial = $v;
    }

    /**
     * Initializes the collInkomens collection.
     *
     * By default this just sets the collInkomens collection to an empty array (like clearcollInkomens());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInkomens($overrideExisting = true)
    {
        if (null !== $this->collInkomens && !$overrideExisting) {
            return;
        }

        $collectionClassName = InkomenTableMap::getTableMap()->getCollectionClassName();

        $this->collInkomens = new $collectionClassName;
        $this->collInkomens->setModel('\Model\Custom\NovumBri\Inkomen');
    }

    /**
     * Gets an array of ChildInkomen objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildInkomen[] List of ChildInkomen objects
     * @throws PropelException
     */
    public function getInkomens(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collInkomensPartial && !$this->isNew();
        if (null === $this->collInkomens || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collInkomens) {
                // return empty collection
                $this->initInkomens();
            } else {
                $collInkomens = ChildInkomenQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collInkomensPartial && count($collInkomens)) {
                        $this->initInkomens(false);

                        foreach ($collInkomens as $obj) {
                            if (false == $this->collInkomens->contains($obj)) {
                                $this->collInkomens->append($obj);
                            }
                        }

                        $this->collInkomensPartial = true;
                    }

                    return $collInkomens;
                }

                if ($partial && $this->collInkomens) {
                    foreach ($this->collInkomens as $obj) {
                        if ($obj->isNew()) {
                            $collInkomens[] = $obj;
                        }
                    }
                }

                $this->collInkomens = $collInkomens;
                $this->collInkomensPartial = false;
            }
        }

        return $this->collInkomens;
    }

    /**
     * Sets a collection of ChildInkomen objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $inkomens A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setInkomens(Collection $inkomens, ConnectionInterface $con = null)
    {
        /** @var ChildInkomen[] $inkomensToDelete */
        $inkomensToDelete = $this->getInkomens(new Criteria(), $con)->diff($inkomens);


        $this->inkomensScheduledForDeletion = $inkomensToDelete;

        foreach ($inkomensToDelete as $inkomenRemoved) {
            $inkomenRemoved->setPersoon(null);
        }

        $this->collInkomens = null;
        foreach ($inkomens as $inkomen) {
            $this->addInkomen($inkomen);
        }

        $this->collInkomens = $inkomens;
        $this->collInkomensPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Inkomen objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Inkomen objects.
     * @throws PropelException
     */
    public function countInkomens(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collInkomensPartial && !$this->isNew();
        if (null === $this->collInkomens || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collInkomens) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getInkomens());
            }

            $query = ChildInkomenQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collInkomens);
    }

    /**
     * Method called to associate a ChildInkomen object to this object
     * through the ChildInkomen foreign key attribute.
     *
     * @param  ChildInkomen $l ChildInkomen
     * @return $this|\Model\Custom\NovumBri\Persoon The current object (for fluent API support)
     */
    public function addInkomen(ChildInkomen $l)
    {
        if ($this->collInkomens === null) {
            $this->initInkomens();
            $this->collInkomensPartial = true;
        }

        if (!$this->collInkomens->contains($l)) {
            $this->doAddInkomen($l);

            if ($this->inkomensScheduledForDeletion and $this->inkomensScheduledForDeletion->contains($l)) {
                $this->inkomensScheduledForDeletion->remove($this->inkomensScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildInkomen $inkomen The ChildInkomen object to add.
     */
    protected function doAddInkomen(ChildInkomen $inkomen)
    {
        $this->collInkomens[]= $inkomen;
        $inkomen->setPersoon($this);
    }

    /**
     * @param  ChildInkomen $inkomen The ChildInkomen object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removeInkomen(ChildInkomen $inkomen)
    {
        if ($this->getInkomens()->contains($inkomen)) {
            $pos = $this->collInkomens->search($inkomen);
            $this->collInkomens->remove($pos);
            if (null === $this->inkomensScheduledForDeletion) {
                $this->inkomensScheduledForDeletion = clone $this->collInkomens;
                $this->inkomensScheduledForDeletion->clear();
            }
            $this->inkomensScheduledForDeletion[]= clone $inkomen;
            $inkomen->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->bsn = null;
        $this->voornaam = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collHuurtoeslags) {
                foreach ($this->collHuurtoeslags as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collKindgebondenbudgets) {
                foreach ($this->collKindgebondenbudgets as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collZorgtoeslags) {
                foreach ($this->collZorgtoeslags as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collKinderpovangtoeslags) {
                foreach ($this->collKinderpovangtoeslags as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collInkomens) {
                foreach ($this->collInkomens as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collHuurtoeslags = null;
        $this->collKindgebondenbudgets = null;
        $this->collZorgtoeslags = null;
        $this->collKinderpovangtoeslags = null;
        $this->collInkomens = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PersoonTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
