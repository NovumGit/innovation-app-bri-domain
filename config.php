<?php

if(isset($_SERVER['IS_DEVEL']))
{
    $aConfig = [
        'PROTOCOL' => 'http',
        'ADMIN_PROTOCOL' => 'http',
        'CUSTOM_FOLDER' => 'NovumBri',
        'ABSOLUTE_ROOT' => '/home/anton/Documents/sites/hurah',
        'DOMAIN' => 'belastingdienst.demo.novum.nuidev.nl',
        'DATA_DIR' => '../'
    ];
}
else
{
    $aConfig = [
        'PROTOCOL' => 'https',
        'ADMIN_PROTOCOL' => 'https',
        'CUSTOM_FOLDER' => 'NovumBri',
        'DOMAIN' => 'belastingdienst.demo.novum.nu',
        'ABSOLUTE_ROOT' => '/home/nov_bri/platform/system',
        'DATA_DIR' => '/home/nov_bri/platform/data'
    ];
}

$aConfig['CUSTOM_NAMESPACE'] = 'NovumBri';

return $aConfig;
