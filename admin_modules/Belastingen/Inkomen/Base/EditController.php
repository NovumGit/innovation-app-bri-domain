<?php
namespace AdminModules\Custom\NovumBri\Belastingen\Inkomen\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumBri\Inkomen\CrudInkomenManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumBri\Belastingen\Inkomen instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudInkomenManager();
	}


	public function getPageTitle(): string
	{
		return "Inkomstenbelasting";
	}
}
