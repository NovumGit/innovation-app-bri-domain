<?php
namespace AdminModules\Custom\NovumBri\Belastingen\Inkomen\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\Custom\NovumBri\Inkomen\CrudInkomenManager;
use Crud\FormManager;
use Model\Custom\NovumBri\Inkomen;
use Model\Custom\NovumBri\InkomenQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumBri\Belastingen\Inkomen instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Inkomstenbelasting";
	}


	public function getModule(): string
	{
		return "Inkomen";
	}


	public function getManager(): FormManager
	{
		return new CrudInkomenManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return InkomenQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof Inkomen){
		    LogActivity::register("Belastingen", "Inkomstenbelasting verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Inkomstenbelasting verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Inkomstenbelasting niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Inkomstenbelasting item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
