<?php
namespace AdminModules\Custom\NovumBri\Belastingen\Inkomen;

/**
 * Skeleton subclass for drawing a list of Inkomen records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class EditController extends Base\EditController
{
}
