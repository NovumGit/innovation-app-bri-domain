<?php
namespace AdminModules\Custom\NovumBri\Belastingen;

use AdminModules\ModuleConfig;
use Core\Translate;

final class Config extends ModuleConfig
{
	public function isEnabelable(): bool
	{
		return true;
	}


	public function getModuleTitle(): string
	{
		return Translate::fromCode("Belastingen");
	}
}

// Optie 1. Code in de juiste mappen plaatsen
// Optie 2. Composer gebruiken om mappen als library te installeren
