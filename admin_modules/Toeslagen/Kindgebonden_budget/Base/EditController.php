<?php
namespace AdminModules\Custom\NovumBri\Toeslagen\Kindgebonden_budget\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumBri\Kindgebondenbudget\CrudKindgebondenbudgetManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumBri\Toeslagen\Kindgebonden_budget instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudKindgebondenbudgetManager();
	}


	public function getPageTitle(): string
	{
		return "Kindgebonden budget";
	}
}
