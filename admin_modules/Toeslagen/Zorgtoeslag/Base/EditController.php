<?php
namespace AdminModules\Custom\NovumBri\Toeslagen\Zorgtoeslag\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumBri\Zorgtoeslag\CrudZorgtoeslagManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumBri\Toeslagen\Zorgtoeslag instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudZorgtoeslagManager();
	}


	public function getPageTitle(): string
	{
		return "Zorgtoeslag";
	}
}
