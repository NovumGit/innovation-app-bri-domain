<?php
namespace AdminModules\Custom\NovumBri\Toeslagen\Kinderpovangtoeslag\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumBri\Kinderpovangtoeslag\CrudKinderpovangtoeslagManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumBri\Toeslagen\Kinderpovangtoeslag instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudKinderpovangtoeslagManager();
	}


	public function getPageTitle(): string
	{
		return "Kinderopvangtoeslag";
	}
}
