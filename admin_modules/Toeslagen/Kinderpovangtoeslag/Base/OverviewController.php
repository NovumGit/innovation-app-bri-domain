<?php
namespace AdminModules\Custom\NovumBri\Toeslagen\Kinderpovangtoeslag\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\Custom\NovumBri\Kinderpovangtoeslag\CrudKinderpovangtoeslagManager;
use Crud\FormManager;
use Model\Custom\NovumBri\Kinderpovangtoeslag;
use Model\Custom\NovumBri\KinderpovangtoeslagQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumBri\Toeslagen\Kinderpovangtoeslag instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Kinderopvangtoeslag";
	}


	public function getModule(): string
	{
		return "Kinderpovangtoeslag";
	}


	public function getManager(): FormManager
	{
		return new CrudKinderpovangtoeslagManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return KinderpovangtoeslagQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof Kinderpovangtoeslag){
		    LogActivity::register("Toeslagen", "Kinderopvangtoeslag verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Kinderopvangtoeslag verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Kinderopvangtoeslag niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Kinderopvangtoeslag item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
