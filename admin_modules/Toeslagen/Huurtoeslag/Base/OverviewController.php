<?php
namespace AdminModules\Custom\NovumBri\Toeslagen\Huurtoeslag\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\Custom\NovumBri\Huurtoeslag\CrudHuurtoeslagManager;
use Crud\FormManager;
use Model\Custom\NovumBri\Huurtoeslag;
use Model\Custom\NovumBri\HuurtoeslagQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumBri\Toeslagen\Huurtoeslag instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Huurtoeslag";
	}


	public function getModule(): string
	{
		return "Huurtoeslag";
	}


	public function getManager(): FormManager
	{
		return new CrudHuurtoeslagManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return HuurtoeslagQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof Huurtoeslag){
		    LogActivity::register("Toeslagen", "Huurtoeslag verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Huurtoeslag verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Huurtoeslag niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Huurtoeslag item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
