<?php
namespace AdminModules\Custom\NovumBri\Toeslagen\Huurtoeslag\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumBri\Huurtoeslag\CrudHuurtoeslagManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumBri\Toeslagen\Huurtoeslag instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudHuurtoeslagManager();
	}


	public function getPageTitle(): string
	{
		return "Huurtoeslag";
	}
}
