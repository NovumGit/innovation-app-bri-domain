<?php
return [
    ['is_deleted' => 0, 'description' => 'Nederlands',  'locale_code' => 'nl_NL', 'is_enabled_cms' => 1, 'is_enabled_webshop' => 1, 'is_default_cms' => 0, 'is_default_webshop' => 1, 'flag_icon' => 'NL', 'shop_url_prefix' => 'nl'],
    ['is_deleted' => 0, 'description' => 'English',  'locale_code' => 'en_US', 'is_enabled_cms' => 1, 'is_enabled_webshop' => 1, 'is_default_cms' => 1, 'is_default_webshop' => 0, 'flag_icon' => 'GB', 'shop_url_prefix' => 'en']
];
