<?php
return [
    ['Tom', 'Sweegs', 'Admin'],
    ['Wesley', 'Visser', 'Admin'],
    ['Anton', 'Boutkam', 'Admin'],
    ['Jeroen', 'Vonk', 'Admin'],
    ['Johanna', 'Glas', 'Admin'],
    ['Lysbet', 'Dekker', 'Admin'],
    ['Matthijs', 'Bruinsma', 'Admin'],
    ['Matthijs', 'Goense', 'Admin'],
    ['Renee', 'Verhoek', 'Admin'],
    ['Tessa', 'Mulder', 'Admin'],
    ['Tim', 'van.Dijk', 'Admin'],
    ['Willem', 'jan.westra.hoekzema', 'Admin'],
];
